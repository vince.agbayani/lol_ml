#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 16:52:13 2017

@author: user
"""
import numpy as np

class neural_net(object):
    def _init_(self):
        self.layer1_size = 2
        self.layer3_size = 1
        self.layer2_size = 3
        self.w1 = np.random.randn(self.layer1_size, self.layer2_size)
        self.w2 = np.random.randn(self.layer2_size, self.layer3_size)
        
   
    def sigmoid(self, z):
        return 1/(1+np.exp(-z))
    
    def forward(self, x):
        self.z2 = np.dot(x, self.w1)
        self.a2 = self.sigmoid(self.z2)
        self.z3 = np.dot(self.a2, self.w2)
        res = self.sigmoid(self.z3)
        return res
    



x = np.random.randn(3,2)    
ayy = neural_net()
print(ayy.forward(x))
