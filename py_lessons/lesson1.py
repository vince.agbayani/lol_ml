#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 14:29:44 2017

@author: user
"""
import seaborn as sns
import random
import matplotlib.pyplot as plt
import pandas as pd

df = pd.DataFrame()

df['x'] = random.sample(range(1,101),10)
df['y'] = random.sample(range(1,101),10)

#sns.lmplot('x','y',data=df,fit_reg=True)


#sns.kdeplot(df.y, shade=True, color='r')

#plt.hist(df.x,alpha=.3)

#sns.rugplot(df.y)

#sns.kdeplot(df.y,df.x,shade=True, n_levels=30)

#sns.boxplot([df.y,df.x])

#sns.violinplot([df.y,df.x],orient='v')

#sns.heatmap([df.x,df.y],annot=True,fmt='d')

#sns.clustermap(df)