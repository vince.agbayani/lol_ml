#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 11:28:09 2017

@author: user


import pandas as pd
import matplotlib.pyplot as plt

df=pd.DataFrame({'Users': [ 'Bob', 'Jim', 'Ted', 'Jesus', 'James'],
                 'Score': [10,2,5,6,7],})

df = df.set_index('Users')
df.plot(kind='bar',  title='Scores')

plt.show()





"""
import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv("Team Stats/Summer Split 2017/EUlcs_smr17.csv")

dt=pd.DataFrame({'Teams': df['Team'],
                 'Score': df['W'] - df['L'],})
dt.set_index('Teams')

dt.plot(kind='bar', title='W/L ratio')

plt.plot(df['AGT'])
plt.plot(df['W'])
plt.show()


